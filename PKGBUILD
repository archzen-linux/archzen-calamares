
# Author: CristoDCGomez <cristo@duck.com>
# Thanks to Ezarcher for the base of this Calamares Installer PKGBUILD
# Thanks to @Ruturajn for the customization and his tutorial
pkgname=calamares
pkgver=3.2.61
_pkgver=3.2.61
pkgrel=1
pkgdesc='Distribution-independent installer framework'
arch=('x86_64')
license=(GPL)
url="https://github.com/calamares/calamares"
license=('LGPL')
depends=('cmake' 'kconfig' 'kcoreaddons' 'kiconthemes' 'ki18n' 'kio' 'solid' 'yaml-cpp' 'kpmcore' 'qt5-webengine' 'kwidgetsaddons'
	'icu' 'qt5-location' 'boost-libs' 'qt5-svg' 'qt5ct' 'polkit-qt5' 'gtk-update-icon-cache' 'plasma-framework' 'parted'
	'qt5-xmlpatterns' 'squashfs-tools' 'libpwquality' 'dmidecode' 'kparts' 'kdbusaddons' 'kservice' 'udisks2' 'upower'
	'python-qtpy' 'appstream' 'appstream-qt' 'kdbusaddons' 'python-pyqt5' 'python-yaml')
makedepends=('extra-cmake-modules' 'qt5-tools' 'qt5-translations' 'git' 'boost')

source=("$pkgname-$pkgver.tar.gz::https://github.com/calamares/calamares/releases/download/v$pkgver/calamares-$pkgver.tar.gz")
sha256sums=('SKIP')

prepare() {
	cd ${srcdir}/calamares-${pkgver}
	sed -i -e 's/"Install configuration files" OFF/"Install configuration files" ON/' CMakeLists.txt

	# change version
	sed -i -e "s|$pkgver|$_pkgver|g" CMakeLists.txt
	_ver="$pkgver"
	printf 'Version: %s-%s' "${_ver}" "${pkgrel}"
	sed -i -e "s|\${CALAMARES_VERSION_MAJOR}.\${CALAMARES_VERSION_MINOR}.\${CALAMARES_VERSION_PATCH}|${_ver}-${pkgrel}|g" CMakeLists.txt
	sed -i -e "s|CALAMARES_VERSION_RC 1|CALAMARES_VERSION_RC 0|g" CMakeLists.txt

	# Change the text displayed during updating the system.
	sed -i 's|Install packages.|Updating System (This may take some time).|g' ./src/modules/packages/main.py
}

build() {
	cd ${srcdir}/calamares-${pkgver}

	mkdir -p build
	cd build
	cmake .. \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=/usr/lib \
		-DWITH_PYTHONQT:BOOL=ON \
		-DBoost_NO_BOOST_CMAKE=ON \
		-DSKIP_MODULES="webview interactiveterminal initramfs \
                              dracut dracutlukscfg initramfscfg \
                              dummyprocess dummypython dummycpp \
                              dummypythonqt services-openrc"
	make
	#-DWITH_KF5DBus=OFF \
}

package() {
	cd ${srcdir}/calamares-${pkgver}/build
	make DESTDIR="$pkgdir" install
	sed -i 's|pkexec calamares|/etc/calamares/launch_calamares.sh|' ../calamares.desktop
	install -Dm644 "../calamares.desktop" "$pkgdir/usr/share/applications/calamares.desktop"
}
